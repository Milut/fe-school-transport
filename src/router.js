import Vue from "vue";
import Router from "vue-router";
import Login from "./components/Login";
import Routes from "./components/Routes";
import Stop from "./components/Stop";
import Stops from "./components/Stops";
import Transport from "./components/Transport";
import Transports from "./components/Transports";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Transport",
      component: Transport
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/routes",
      name: "Routes",
      component: Routes
    },
    {
      path: "/stop",
      name: "Stop",
      component: Stop
    },
    {
      path: "/stops",
      name: "Stops",
      component: Stops
    },
    {
      path: "/transports",
      name: "Transports",
      component: Transports
    }
  ]
});
